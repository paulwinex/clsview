from django.conf.urls import url
from django.contrib import admin
from django.views.decorators.csrf import csrf_exempt

from app1.views import IndexView, LoginView, ExampleAjaxView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^list/$', csrf_exempt(ExampleAjaxView.as_view()), name='ajax'),

]
