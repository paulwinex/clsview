from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.views.generic import TemplateView
from django.http import HttpResponseForbidden, Http404, JsonResponse
from django.shortcuts import redirect, reverse


class IndexView(TemplateView):
    template_name = 'app1/index.html'


class LoginView(TemplateView):
    template_name = 'app1/login.html'

    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)
        return self.render_to_response(ctx)

    def post(self, request):
        # ctx = self.get_context_data()
        username = request.POST.get('username')
        pwd = request.POST.get('pwd')
        user = authenticate(username=username, password=pwd)
        if user:
            login(request, user)
            return redirect(reverse('index'))
        else:
            return HttpResponseForbidden('ERROR')
        data = dict(
            status=1,
            error='',
            result=''
        )
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        ctx = super(LoginView, self).get_context_data(**kwargs)
        ctx['value'] = '123'
        return ctx


class ExampleAjaxView(TemplateView):
    template_name = 'app1/ajax.html'

    def post(self, request):
        print(request.POST)
        ctx = self.get_context_data()
        ctx['items'] = range(10)
        return self.render_to_response(ctx)
